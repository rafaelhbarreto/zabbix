<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/zabbixreport', function (){
   return view('welcome');
})-> name('zabbixreport');

Route::get('/date?grid=Dates&advanced-search=true')-> name('search');

Route::get('/groups', 'ZabbixGroupController@index')->name('groups');

Route::get('/hosts/{group_id}', 'ZabbixHostController@index')->name('hosts');

Route::get('/items/{host_id}', 'ZabbixItemsController@index')->name('items');

Route::get('item/{item_id}', 'ZabbixItemsController@showForm')->name('form_item');
Route::post('proccessItem', 'ZabbixItemsController@item')->name('item');
Route::get('download/{filename}', 'ZabbixItemsController@download')->name('download');

Route::get('history/{item_id}', 'ZabbixItemsController@history')->name('history');

Route::get('test', 'ZabbixItemsController@test')->name('test');


/**
 * DATAS
 */

Route::get('/ajax-host', 'ZabbixHostController@list')->name('pathHost');

Route::resource('date', 'DateController');

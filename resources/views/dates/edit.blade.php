@extends('template')
@section('title', 'Criação de data')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session('msg'))
                <div class="alert alert-danger">
                    {{session('msg')}}
                </div>
            @endif

            <div class="row">
                <div class="col-xl-12 text-right">
                    <a href="{{route('date.index')}}" class="btn-link">Listar todos</a>
                    <a href="{{route('date.create')}}" class="btn-link">Criar nova</a>
                </div>
            </div>
            
            <form action="{{route('date.update', ['id' => $date->id])}}" method="post">
                {{ csrf_field() }}
                @method('PUT')
                
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="date"> * Data </label>
                            <input type="text" 
                                name="date" 
                                id="datepicker" 
                                class="form-control br-date" 
                                placeholder="dd/mm/yyyy"
                                required
                                value="{{$date->dat_day}}"
                            >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="group"> * Group </label>
                            <select name="group" class="form-control" required id="group">
                                <option value="">--</option>
                                @if(count($groups) > 0 )
                                    @foreach($groups as $group)
                                        @if($date->dat_group == $group->groupid)
                                            <option value="{{$group->groupid}}" selected>{{$group->groupid}} - {{$group->name}}</option>
                                        @else
                                            <option value="{{$group->groupid}}">{{$group->groupid}} - {{$group->name}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="host"> * Host </label>
                            <select name="host" class="form-control" required id="host">
                                <option value="">--</option>
                                @if(count($hosts) > 0 )
                                    @foreach($hosts as $host)
                                        @if($date->dat_host == $host->id)
                                            <option value="{{$host->id}} - {{$host->hostname}}" selected>{{$host->id}} - {{$host->hostname}}</option>
                                        @else
                                            <option value="{{$host->id}} - {{$host->hostname}}">{{$host->id}} - {{$host->hostname}}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="comment" class="d-block">Comentário</label>
                            <textarea name="comment" id="comment" class="form-control" cols="30" rows="10">{{$date->dat_comment}}</textarea>
                        </div>
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="submit" value="Gravar" class="btn btn-primary">
                            <input type="hidden" id="pathLoadHost" value="{{route('pathHost')}}">
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@extends('template')
@section('title', 'Criação de data')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-xl-12">
            
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if(session('msg'))
                <div class="alert alert-danger">
                    {{session('msg')}}
                </div>
            @endif

            <div class="row">
                <div class="col-xl-12 text-right">
                    <a href="{{route('search')}}" class="btn-link">Listar todos</a>
                </div>
            </div>
            
            <form action="{{route('date.store')}}" method="POST">
                {{ csrf_field() }}

                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="date"> * Data </label>
                            <input type="text" 
                                name="date" 
                                id="datepicker" 
                                class="form-control br-date" 
                                placeholder="dd/mm/yyyy"
                                required
                                value="{{old('date')}}"
                            >
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="group"> * Group </label>
                            <select name="group" class="form-control" required id="group">
                                <option value="">--</option>
                                @if(count($groups) > 0 )
                                    @foreach($groups as $group)
                                        <option value="{{$group->groupid}}">{{$group->groupid}} - {{$group->name}}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="host"> * Host </label>
                            <select name="host" class="form-control" required id="host">
                                <option value="">--</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <label for="comment" class="d-block">Comentário</label>
                            <textarea name="comment" id="comment" class="form-control" cols="30" rows="10">{{old('comment')}}</textarea>
                        </div>
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="col">
                        <div class="form-group">
                            <input type="submit" value="Gravar" class="btn btn-primary active" role="button" aria-pressed="true">
                            <input type="hidden" id="pathLoadHost" value="{{route('pathHost')}}">
                            <a href="{{route('zabbixreport')}}" class="btn btn-secondary active" role="button" aria-pressed="true">Voltar</a>                            
                        </div>               
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection
@extends('template')
@section('title', 'Listagem de datas')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-xl-12 text-right">
            <a href="{{route('zabbixreport')}}" class="btn btn-primary btn-sm active" role="button" aria-pressed="true">Nova Data</a>                            
            <a href="{{route('zabbixreport')}}" class="btn btn-secondary btn-sm active" role="button" aria-pressed="true">Página Inicial</a>                            
            
        </div>
        <div class="col-xl-12">
            <div class="alert alert-warning">
                Na busca avançada, usar a data no formato US : Ano-mes-dia Ex.: 2020-12-20
            </div> <br>
            
            {!!$grid->make()!!}
        </div>
    </div>
</div>
@endsection
<!doctype html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Zabbix Api</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    
    <!-- CSS LARAVEL SIMPLEGRID -->
    <link rel="stylesheet" href="{{asset('css/simplegrid.css')}}">
    
</head>
<body>

    @yield('content')


    <script src="{{asset('/js/app.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- JS LARAVEL SIMPLEGRID -->
    <script src="{{asset('js/simplegrid.js')}}"></script>

    <script>
        $(document).ready(function(){

            // mask
            $('.date').mask('00/00/0000 00:00');
            $('.br-date').mask('00/00/0000');

            // datepicker 
            $("#datepicker").datepicker({
                dateFormat: 'dd/mm/yy',
                dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
                dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
                dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
                monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
                monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
                nextText: 'Próximo',
                prevText: 'Anterior'
            });
            
            $('#group').on('change', function() {

                const codGroup = $(this).val();
                const path = $('#pathLoadHost').val(); 
                
                $('#host').empty();
                $("#host").append('<option value="">Carregando ...</option>')

                $.ajax(path, {
                    type: 'GET',  // http method
                    data: { group: codGroup },  // data to submit
                    success: function (data, status, xhr) 
                    {
                        var content  = ''; 
                        $.each(data, function(index, value){
                            $('#host').empty();
                            content = content + '<option value="'+ value.id + ' - ' +value.name +'">'+ value.id + ' - ' +value.name +'</option>';
                            $("#host").append(content);
                        });                        
                    },
                    error: function (jqXhr, textStatus, errorMessage) {
                            alert('Erro ao consultar!');
                    }
                });

            });

        });
    </script>
    
</body>
</html>

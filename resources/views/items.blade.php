@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('groups')}}">Grupos Zabbix</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Items do Host</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <ul>
                    @foreach($items as $item)
                        <li>
                            <a href="{{route('form_item', ['item_id' => $item->itemid])}}">{{$item->itemid}} - {{$item->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection

@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('groups')}}">Grupos Zabbix</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Hosts Cadastrados</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-12">
                <ul>
                    @foreach($hosts as $host)
                        <li>
                            <a href="{{route('items', ['host_id' => $host->hostid])}}">{{$host->hostid}} - {{$host->host}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection

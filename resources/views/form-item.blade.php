@extends('template')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('groups')}}">Grupos Zabbix</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Relátorio do Item do Host</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-8 ml-auto mr-auto">
                
                @if(session('msg'))
                    <div class="alert alert-success" role="alert">
                        {!! session('msg') !!}
                    </div>
                @endif
                <form action="{{route('item')}}" class="form" method="post">
                    {{csrf_field()}}
                    
                    <div class="form-row">
                        <div class="col">
                            <div class="form-group">
                                <label for="from">Data de início - Exemplo: 20/01/2020 08:00</label>
                                <input type="text" name="from" 
                                       class="form-control date" 
                                       required 
                                       placeholder="dd/mm/yyyy H:i"
                                       maxlength="16" 
                                       value="{{old('from')}}"
                                />
                            </div>
                        </div>
                        
                        <div class="col">
                            <div class="form-group">
                                <label for="from">Data de fim - Exemplo: 27/01/2020 18:00</label>
                                <input type="text" name="till" 
                                       class="form-control date" 
                                       required 
                                       placeholder="dd/mm/yyyy H:i"
                                       maxlength="16" 
                                       value="{{old('till')}}"
                                />
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-xl-12 text-center">
                            <input type="hidden" name="id_item" value="{{$id_item}}">
                            <input type="submit" class="btn btn-primary">
                            <a href="{{route('zabbixreport')}}" class="btn btn-secondary active" role="button" aria-pressed="true">Voltar</a>                            
                
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@extends('template')

@section('content')

    <div class="container">
        
        <div class="row">
            <div class="col-xl-12">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Grupos Zabbix</li>
                        <li class="breadcrumb-item"><a href="{{route('zabbixreport')}}">Pagina Incial</a></li>                        
                    </ol>
                </nav>
            </div>
        </div>
        
        <div class="row">
            <div class="col-xl-12">
                <ul>
                    @foreach($groups as $group)
                        <li>
                            <a href="{{route('hosts', ['group_id' => $group->groupid])}}">{{$group->groupid}} - {{$group->name}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>

@endsection

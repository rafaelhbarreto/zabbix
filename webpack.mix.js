const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js([
    'resources/js/app.js',
    'vendor/igorescobar/jquery-mask-plugin/dist/jquery.mask.min.js'
],'public/js')
        .sass('resources/sass/app.scss','public/css');

mix.copy('vendor/rafwell/laravel-simplegrid/public/css/simplegrid.css', 'public/css/simplegrid.css');
mix.copy('vendor/rafwell/laravel-simplegrid/public/js/simplegrid.js', 'public/js/simplegrid.js');

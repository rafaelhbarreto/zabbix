<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatesHolidaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dates_holidays', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dat_day')->comment('Data que será excluída');
            $table->integer('dat_host'); 
            $table->string('dat_host_name',100); 
            $table->integer('dat_group'); 
            $table->text('dat_comment')->nullable(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dates_holidays');
    }
}

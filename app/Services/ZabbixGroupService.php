<?php

namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

use App\Repositories\DateRepository; 


class ZabbixGroupService {

    /**
     * The ZabbixApi instance.
     *
     * @var \Becker\Zabbix\ZabbixApi
     */
    protected $zabbix;

    /**
     * Layer service
     * @var ZabbixGroupService
     */
    protected $service;
    
    public function __construct() {
        $this->zabbix = app('zabbix');        
    }

    /**
     * Retorna uma coleção de Grupos
     *
     * @return array|\Illuminate\Support\Collection
     * @throws \Becker\Zabbix\ZabbixException
     */
    public function getGroups() {
        return $this->zabbix->hostgroupGet([
            'output' => 'extend',
            'real_hosts' => 1
        ]);

        // Or, if you want to use Laravel Collections

        return collect($this->zabbix->hostgroupGet())->map(function ($item) {
                    return [
                        'name' => strtoupper($item->name)
                    ];
                });
    }

    public function getHosts($groupId) {
        return $this->zabbix->hostGet([
                    'groupids' => $groupId,
                    'output' => 'extend'
        ]);
    }
    
    /**
     * 
     * @param int $hostId
     * @return type
     */
    public function getHostName(int $hostId)
    {
        $result = $this->zabbix->hostGet([
            'hostids' => $hostId,
            'output' => 'extend'
        ]);
        
        
        return array_shift($result)->host; 
    }

    public function getItems($hostId) {
        return $this->zabbix->itemGet([
                    'hostids' => $hostId,
                    'output' => 'extend'
        ]);
    }

    public function getItem($itemId) {
        return $this->zabbix->itemGet([
                    'itemids' => $itemId,
                    'output' => 'extend',
        ]);
    }

    /**
     * Returns a history of an item
     * 
     * @param int $itemId
     * @param Timestamp $from
     * @param Timestamp $till
     * @return array
     */
    public function getHistory($itemId, $from, $till) {
        return $this->zabbix->historyGet([
                    'itemids' => $itemId,
                    'history' => 3,
                    'output' => 'extend',
                    'sortfield' => 'clock',
                    'sortorder' => 'DESC',
                    'time_from' => $from,
                    'time_till' => $till
        ]);
    }

    public function calc(Carbon $from, Carbon $to, int $idItem) 
    {
        
        $final_result = []; 
        $arr_item = $this->getItem($idItem);
        $item = array_shift($arr_item);  
        $host_id = $item->hostid;
        
        if ($to->isSameDay($from)) {
            if ($this->isValidDate($from, $host_id)) {
                
                $new_to = $this->generateNewTo($from, $to);
                $results = $this->getHistory($idItem, $from->timestamp, $new_to->timestamp);
                
                if($this->isValidDate($from)) {
                    $final_result = [$from->format('d/m/Y') => $this->avgDay($results)]; 
                }
                
            }
        } else {
            
            
            if ($to->isBefore($from) > 0) {
                throw new \Exception('Datas inválidas!');
            } else {

                $diff_in_days = $from->diffInDays($to);
                
                $arr_results = []; 
                
                
                for ($i = 0; $i <= $diff_in_days; $i++) {
                    
                    $new_to = $this->generateNewTo($from, $to);
                    $results = $this->getHistory($idItem, $from->timestamp, $new_to->timestamp);
                    
                    if($this->isValidDate($from, $host_id)) {
                        $arr_results[$from->format('d/m/Y')] = $this->avgDay($results);
                    }

                    $from->addDay(1); 
                }
                
                $final_result = $arr_results; 
            }
        }
        
        $tmp = $final_result; 
        $sum_total = 0; 
        $sum_avg = 0; 
        
        if(count($final_result) > 0) {
            foreach($final_result as $key => $result) {
                $sum_total += $result['total'];
                $sum_avg += $result['avg'];
            }
            
            $final_result = [
                'sum_total' => $sum_total,
                'sum_avg' => $sum_avg,
                'days' => count($final_result)
            ];
        }
        
        return [
            'total' => $final_result,
            'media_diaria' => $tmp
        ];
    }
    
    /**
     * Storages the content on file
     * 
     * @param array $result
     */
    public function putToFile(array $result, $fileName)
    {
        $fileName = $fileName.'.csv'; 
        
        $dias = $result['media_diaria'];
        $total = $result['total']; 
        
        $content = 'Dia;Total do Dia (em Gbps) ;Media do Dia (em Mbps)'.PHP_EOL; 
        if(count($dias) > 0 ) {
            foreach($dias as $key   => $value) {
                $content .= $key.';'.$value['total'].';'.$value['avg'].PHP_EOL;
            }
        }
        
        $content .= ''.PHP_EOL;
        $content .= ''.PHP_EOL;
        
        $content .= 'Soma dos Totais do Periodo (em Gbps) ;Soma das Medias do Periodo (em Mbps) ;Qtd. Dias'.PHP_EOL;
        $content .= $total['sum_total'].';'.$total['sum_avg'].';'.$total['days'];
        
        Storage::put($fileName, $content);
    }
    
    /**
     * 
     * @param string $filename
     * @return type
     */
    public function download($filename)
    {
        $ext = '.csv';
        $filename = $filename.'.csv';
        
        return Storage::download($filename);
    }
    
    /**
     * Generate a new till 
     * 
     * @param DateTime $from
     * @param DateTime $to
     * @return DateTime $new_to
     */
    private function generateNewTo($from, $till) {
        $start_day = $from->format('Y-m-d');
        $start_hour = $from->format('H:i:s');

        $end_day = $till->format('Y-m-d');
        $end_hour = $till->format('H:i:s');

        // definir o horário de entrada e saída do mesmo dia

        return $new_to = Carbon::createFromFormat('Y-m-d H:i:s', $start_day . ' ' . $end_hour);
    }

    private function avgDay(array $results) {

        $result = ['total' => 0, 'avg' => 0];
        $count = count($results);

        if ($count > 0) {
            foreach ($results as $res) {
                $result['total'] += $res->value;
            }

            $result['avg'] = $result['total'] / $count;
            $result['total'] = round((float) ($result['total'] / 1000000000), 9);
            $result['avg'] = round((float) ($result['avg'] / 1000000), 4);
        }
        
        return $result;
    }

    public function isValidDate($date, $host_id = null) {
        if (!$this->isHoliday($date, $host_id)) {
            return true;
        }

        return false;
    }

    /**
     * Verify if the given is a holiday
     *
     * @param Carbon $date
     */
    private function isHoliday(Carbon $date, $host_id) {

        $holidays = DateRepository::getHolidayAndWeekend($host_id); 
        foreach ($holidays as $holy) {
            $date_holy = Carbon::createFromFormat('d/m/Y', $holy->dat_day);
            if ($date->isSameDay($date_holy)) {
                return true;
            }
        }

        return false;
    }

}

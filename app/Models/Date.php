<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon; 

class Date extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dates_holidays';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    public function setDatDayAttribute($value)
    {
        // 29/04/1988 => 1988-04-29
        $this->attributes['dat_day'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d'); 
    }
    
    public function getDatDayAttribute($value)
    {
        // 1988-04-29 => 29/04/1988
        return Carbon::createFromFormat('Y-m-d', $value)->format('d/m/Y'); 
    }
}

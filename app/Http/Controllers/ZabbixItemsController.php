<?php

namespace App\Http\Controllers;

use App\Services\ZabbixGroupService;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class ZabbixItemsController extends Controller
{

    /**
     * Dados que serão passados para view
     *
     * @var array
     */
    protected $data;

    /**
     * Camada de serviço da aplicação
     *
     * @var ZabbixGroupService
     */
    protected $service;

    /**
     * Create a new Zabbix API instance.
     *
     * @return void
     */
    public function __construct(ZabbixGroupService $service)
    {
        $this->service = $service;
    }

    /**
     * Retorna uma view com a lista de todos os itens
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($host_id)
    {
        $this->data['items'] = $this->service->getItems($host_id);

        return view('items')->with($this->data);
    }

    public function showForm($id_item)
    {
        $this->data['id_item'] = $id_item;
        return view('form-item')->with($this->data);
    }
    
    public function item(Request $request)
    {
        try {

            $item_id = $request->id_item; 
            $from = $request->from.':00';
            $till = $request->till.':00'; 
            
            // get the item 
            $item = $this->service->getItem($item_id);
            
            // data from view
            $dt_start = Carbon::createFromFormat('d/m/Y H:i:s', $from);
            $dt_end = Carbon::createFromFormat('d/m/Y H:i:s', $till);
            $date_file = ' '.$dt_start->format('d-m-Y').' a '.$dt_end->format('d-m-Y');
            
            // values of days and avg
            $values = $this->service->calc($dt_start, $dt_end, $item_id);
            
            // get the host name
            $host_id = array_shift($item)->hostid;
            $host_name = $this->service->getHostName($host_id); 
            $file_name = $host_name.' '.$date_file;
            
            // put the content on file.
            $this->service->putToFile($values, $file_name);
            
            $link = '<a href='.route('download', ['filename' => $file_name]).'> Download </a>';
            return redirect()->route('form_item', ['item_id' => $item_id])
                    ->withInput()
                    ->with('msg', "Relatório gerado! {$link}"); 
                    
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
    
    public function download($fileName) {
        return $this->service->download($fileName);
    }

    public function history($item_id, $history = null)
    {
        return $history = $this->service->getHistory($item_id);
    }

    public function test()
    {
        $this->service->test();
    }

}

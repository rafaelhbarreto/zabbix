<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestController extends Controller
{
    /**
     * The ZabbixApi instance.
     *
     * @var \Becker\Zabbix\ZabbixApi
     */
    protected $zabbix;

    /**
     * Create a new Zabbix API instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->zabbix = app('zabbix');
    }

    /**
     * Get all the Zabbix host groups.
     *
     * @return array
     */
    public function index()
    {
        return $this->zabbix->hostgroupGet([
            'output' => 'extend'
        ]);

        // Or, if you want to use Laravel Collections

        return collect($this->zabbix->hostgroupGet())->map(function ($item) {
            return [
                'name' => strtoupper($item->name)
            ];
        });
    }
}

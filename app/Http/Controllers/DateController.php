<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\DateRepository;
use App\Services\ZabbixGroupService;
use App\Http\Requests\DateRequest;
use Rafwell\Simplegrid\Grid;
use App\Models\Date;

class DateController extends Controller {

    protected $data = [];
    private $repository;
    private $zabbixService;

    public function __construct(DateRepository $repository, ZabbixGroupService $zabbixService) {
        $this->repository = $repository;
        $this->zabbixService = $zabbixService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return $this->list();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->data['groups'] = $this->zabbixService->getGroups();
        return view('dates.create')->with($this->data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DateRequest $request) {
        if ($request->isMethod('post')) {

            if ($this->repository->isAvailable($request->date, $request->host)) {
                $date = $this->repository->create($request);
                
                 $msg = 'Gravado com sucesso!';
                return redirect()->route('date.create')
                                ->with('msg', $msg)
                                ->withInput();
            } else {
                $msg = 'Essa data já foi cadastrada para este host!';
                return redirect()->route('date.create')
                                ->with('msg', $msg)
                                ->withInput();
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $date = $this->repository->findById($id);

        $this->data['groups'] = $this->zabbixService->getGroups();
        $this->data['hosts'] = array_map(function($day) {
            $tmp = new \stdClass();
            $tmp->id = $day->hostid;
            $tmp->hostname = $day->host;
            return $tmp;
        }, $this->zabbixService->getHosts($date->dat_group));


        $this->data['date'] = $date;
        return view('dates.edit')->with($this->data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $date = $this->repository->findById($id);
        if ($request->date !== $date->dat_day) {
            if ($this->repository->isAvailable($request->date, $request->host)) {
                $this->repository->update($id, $request);

                $msg = 'Atualizado com sucesso!';
                return redirect()->route('date.edit', ['id' => $id])
                                ->with('msg', $msg)
                                ->withInput();
            } else {
                $msg = 'Essa data já foi cadastrada para este host!';
                return redirect()->route('date.edit', ['id' => $id])
                                ->with('msg', $msg)
                                ->withInput();
            }
        } else {
            $this->repository->update($id, $request);
             $msg = 'Atualizado com sucesso!';
             return redirect()->route('date.edit', ['id' => $id])
                             ->with('msg', $msg)
                             ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->repository->destroy($id);
        return redirect()->route('date.index');
    }

    public function list() {
        $Grid = new Grid(Date::query(), 'Dates');

        $Grid->fields([
                    'dat_day' => 'Data',
                    'dat_host' => 'ID. Host',
                    'dat_host_name' => 'Hostname',
                    'dat_comment' => 'Comentário',
                ])
                ->actionFields([
                    'id' //The fields used for process actions. those not are showed 
                ])
                ->advancedSearch([
                    'dat_day' => [
                        'type' => 'text',
                        'label' => 'Data'
                    ],
                    'dat_host' => [
                        'type' => 'text',
                        'label' => 'ID. Host'
                    ]
        ]);

        $Grid->action('Editar', 'date/{id}/edit')
                ->action('Deletar', 'date/{id}', [
                    'confirm' => 'Essa é uma ação irreversível. Deseja continuar? ',
                    'method' => 'DELETE',
        ]);

        return view('dates.list', ['grid' => $Grid]);
    }

}

<?php

namespace App\Http\Controllers;

use App\Services\ZabbixGroupService;
use Illuminate\Http\Request;

use App\Http\Requests;

class ZabbixGroupController extends Controller
{

    /**
     * Dados que serão passados para view
     *
     * @var array
     */
    protected $data;

    /**
     * Camada de serviço da aplicação
     *
     * @var ZabbixGroupService
     */
    protected $service;

    /**
     * Create a new Zabbix API instance.
     *
     * @return void
     */
    public function __construct(ZabbixGroupService $service)
    {
        $this->service = $service;
    }

    /**
     * Get all the Zabbix host groups.
     *
     * @return array
     */
    public function index()
    {
        $this->data['groups'] = $this->service->getGroups();

        return view('groups')->with($this->data);
    }
}

<?php

namespace App\Http\Controllers;

use App\Services\ZabbixGroupService;
use Illuminate\Http\Request;

class ZabbixHostController extends Controller {

    /**
     * Dados que serão passados para view
     *
     * @var array
     */
    protected $data;

    /**
     * Camada de serviço da aplicação
     *
     * @var ZabbixGroupService
     */
    protected $service;

    /**
     * Create a new Zabbix API instance.
     *
     * @return void
     */
    public function __construct(ZabbixGroupService $service) {
        $this->service = $service;
    }

    /**
     * Retorna uma view com a lista de todos os hosts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($group_id) {
        $this->data['hosts'] = $this->service->getHosts($group_id);
        return view('hosts')->with($this->data);
    }

    public function list(Request $request) {
        
        if ($request->has('group')) {
            
            $hosts = $this->service->getHosts($request->group); 
            
            $new_hosts = array_map(function($host){
                $tmp = [
                    'id' => $host->hostid,
                    'name' => $host->name
                ];
                
                return $tmp; 
            }, $hosts);
            
            return response()->json($new_hosts); 
            
        } else {
            return response()->json([]);
        }
    }

}

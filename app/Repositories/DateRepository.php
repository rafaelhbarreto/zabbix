<?php

namespace App\Repositories;

use App\Models\Date;
use Illuminate\Support\Facades\Request;
use Carbon\Carbon; 
use Illuminate\Support\Str; 
use App\Services\ZabbixGroupService;

class DateRepository {

    protected $zabbixService;
    
    function __construct(ZabbixGroupService $service)  
    {
        $this->zabbixService = $service; 
    }
    
    public function findById($id) {
        return Date::findOrFail($id);
    }

    public function isAvailable($date, $host_id) {
        
        $usDate = Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
        
        $qtd = Date::where('dat_day', $usDate)
                ->where('dat_host', $host_id)
                ->count();
        
        if($qtd > 0)
            return false;
        return true;
    }
    
    public function create($data) {
        
        // separar o que é ID e o que é no host_name
        
        $host_id = trim(Str::before($data->host, '-'));
        $host_name = trim(Str::after($data->host, '-'));
        
        $date = new Date();
        $date->dat_host = $host_id;
        $date->dat_host_name = $host_name;
        $date->dat_group = $data->group;
        $date->dat_day = $data->date;
        $date->dat_comment = $data->comment;
        $date->save();
        
        return $date;
    }

    public function update($id, $data) {
        
        $date = $this->findById($id);
        
        $host_id = trim(Str::before($data->host, '-'));
        $host_name = trim(Str::after($data->host, '-'));
        
        $date->dat_host = $host_id;
        $date->dat_host_name = $host_name;
        $date->dat_group = $data->group;
        $date->dat_day = $data->date;
        $date->dat_comment = $data->comment;
        
        $date->save();
        
        return $date;
    }

    public function destroy($id) {
        $date = $this->findById($id);
        return $date->delete();
    }
    
    static public function getHolidayAndWeekend($host_id) 
    {
        return Date::where('dat_host', $host_id)->get();
    }

}
